BRANCH="$1"
MODID="$2"
MODNAME="$3"
MODVERSION="$4"
MODCLASS="$5"

git clone git@github.com:cam72cam/ModCore.git --branch $BRANCH
git clone git@github.com:cam72cam/TrackAPI.git --branch $BRANCH

sed -i build.gradle -e "s/#MODID#/$MODID/" -e "s/#MODNAME#/$MODNAME/" -e "s/#MODVERSION#/$MODVERSION/"
mkdir -p src/main/java/cam72cam/mod/$MODID/
mv src/main/java/cam72cam/mod/Mod.java src/main/java/cam72cam/mod/$MODID/Mod.java
sed -i src/main/java/cam72cam/mod/$MODID/Mod.java -e "s/#MODID#/$MODID/" -e "s/#MODNAME#/$MODNAME/" -e "s/#MODVERSION#/$MODVERSION/" -e "s/#MODCLASS#/$MODCLASS/"
rm .git -rf
./gradlew idea
